package WordCounter;

import java.util.HashMap;

public class WordCounter {
	private String massage;
	HashMap<String, Integer> wordCount = new HashMap<String, Integer>();
	public WordCounter(String s){
		this.massage = s;
	}
	
	public void count(){
		String[] splitedmass = this.massage.split(" ");
		for (int i = 0; i < splitedmass.length; i++) {
            if (!wordCount.containsKey(splitedmass[i])) {
            	wordCount.put(splitedmass[i], 1);
            } else {
            	wordCount.put(splitedmass[i], wordCount.get(splitedmass[i]) + 1);
            }
            
        }
	}
	
	public int hasWord(String word){
		if (wordCount.containsKey(word)) return wordCount.get(word);
		else return 0;
		
	}
}
