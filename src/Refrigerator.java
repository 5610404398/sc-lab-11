import java.util.ArrayList;

public class Refrigerator {
	private final static int SIZE = 5;
	private ArrayList<String> things = new ArrayList<String>();

	public void put(String stuff) throws FullException {
		if (things.size() < SIZE) {
			things.add(stuff);
		}
		else {
			System.err.println("Can't add "+stuff+" Refrigerator is full.");
		}
	}
	
	public String takeOut(String stuff){
		String get = "";
		if (things.contains(stuff)){
			get = things.get(things.indexOf(stuff));
			things.remove(stuff);
			return get;
		}
		else {
			return null;
		}
	}
	
	public String toString(){
		return things.toString();
	}
}
