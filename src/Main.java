public class Main {
	public static void main(String[] args) {
		Refrigerator r = new Refrigerator();
		try {
			r.put("Fish");
			r.put("Coke");
			r.put("Water");
			r.put("Orange juice");
			r.put("Pizza");
			r.put("Cake");
			
			System.out.println(r.toString());
			System.out.println(r.takeOut("Fish"));
			System.out.println(r.toString());
			System.out.println(r.takeOut("Beer"));
			
		} catch (FullException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	
	}
}